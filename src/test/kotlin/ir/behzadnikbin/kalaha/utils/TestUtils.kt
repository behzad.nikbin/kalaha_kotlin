package ir.behzadnikbin.kalaha.utils

import ir.behzadnikbin.kalaha.data.model.Game
import org.junit.jupiter.api.Assertions

/**
 * Utility class to fascinate assertion of game state
 *
 * @author Behzad
 */
class TestUtils {
    companion object {

        fun assertEquals(expectedState: Array<Int>, game: Game) {
            val pits = game.pits
            Assertions.assertEquals(expectedState.size, pits.size)
            for (pitId in 1..pits.size) {
                val pit = pits[pitId]
                Assertions.assertNotNull(pit)
                Assertions.assertEquals(expectedState[pitId - 1], pit?.stoneCount)
            }
        }
    }
}
