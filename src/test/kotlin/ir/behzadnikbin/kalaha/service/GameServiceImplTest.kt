package ir.behzadnikbin.kalaha.service

import ir.behzadnikbin.kalaha.KalahaApplication
import ir.behzadnikbin.kalaha.data.model.Game
import ir.behzadnikbin.kalaha.data.repository.GameRepository
import ir.behzadnikbin.kalaha.dto.GameFinishState
import ir.behzadnikbin.kalaha.dto.GameTurn
import ir.behzadnikbin.kalaha.logics.GameFactory
import ir.behzadnikbin.kalaha.utils.NotAcceptableException
import ir.behzadnikbin.kalaha.utils.TestUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

/**
 * Unit tests for {@link GameService GameService}
 * GameService is a Spring Bean, so it needs Spring context, but it doesn't need web environment
 * Most parts of this class is converted from Java to Kotlin by IntellijIDEA
 *
 * @author Behzad
 */
@SpringBootTest(classes = [KalahaApplication::class], webEnvironment = SpringBootTest.WebEnvironment.NONE)
internal class GameServiceImplTest {

    //TODO HELP: it wasn't working when I put the private vars to constructor and make it val
    @Autowired
    private lateinit var gameService: GameService

    //TODO HELP: I think there must be a specific library for kotlin instead of Mockito
    @MockBean
    private lateinit var gameRepository: GameRepository


    @BeforeEach
    fun beforeEach() {
        mockSaveGame()
    }

    @Test
    fun testCreateGame() {
        val game = gameService.createGame()
        Assertions.assertNotNull(game)
        TestUtils.assertEquals(arrayOf(
                6, 6, 6, 6, 6, 6,
                0,
                6, 6, 6, 6, 6, 6,
                0
        ), game)
        Assertions.assertEquals(GameFinishState.NOT_FINISHED, game.finishedState)
        Assertions.assertEquals(GameTurn.PLAYER1, game.gameTurn)
    }

    @Test
    fun testMovePlayer1NormalGameTurn() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 2)
        Assertions.assertEquals(GameTurn.PLAYER2, game.gameTurn)
    }

    @Test
    fun testMovePlayer2NormalGameTurn() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 8)
        Assertions.assertEquals(GameTurn.PLAYER1, game.gameTurn)
    }

    @Test
    fun testMovePlayer1GameTurnForLastStoneInBigPit() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 3)
        Assertions.assertEquals(GameTurn.PLAYER1, game.gameTurn)
    }

    @Test
    fun testMovePlayer2eGameTurnForLastStoneInBigPit() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                6, 5, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 8)
        Assertions.assertEquals(GameTurn.PLAYER2, game.gameTurn)
    }

    @Test
    fun testMoveCreatedGameVer1() {
        val game = GameFactory.createDefaultGame(6, 6)
        game.gameTurn = GameTurn.PLAYER1
        mockFindGame(game)
        gameService.move(1L, 1)
        TestUtils.assertEquals(arrayOf(
                0, 7, 7, 7, 7, 7,
                1,
                6, 6, 6, 6, 6, 6,
                0
        ), game)
    }

    @Test
    fun testMoveCreatedGameVer2() {
        val game = GameFactory.createDefaultGame(6, 6)
        game.gameTurn = GameTurn.PLAYER2 //  it never happens in current game flow, but it must be supported.
        mockFindGame(game)
        gameService.move(1L, 9)
        TestUtils.assertEquals(arrayOf(
                7, 6, 6, 6, 6, 6,
                0,
                6, 0, 7, 7, 7, 7,
                1
        ), game)
    }


    @Test
    fun testMoveMiddleOfGameVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                7, 7, 0, 8, 9, 4,
                0,
                4, 2, 10, 0, 6, 5,
                10
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 8)
        TestUtils.assertEquals(arrayOf(
                7, 7, 0, 8, 9, 4,
                0,
                0, 3, 11, 1, 7, 5,
                10
        ), game)
    }

    @Test
    fun testMoveMiddleOfGameVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 7, 6,
                2
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 12)
        TestUtils.assertEquals(arrayOf(
                6, 8, 1, 9, 7, 6,
                3,
                6, 6, 4, 6, 0, 7,
                3
        ), game)
    }

    @Test
    fun testMoveSkipOpponentBigPitVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 6, 0, 8, 1, 21,
                3,
                6, 1, 4, 2, 7, 6,
                2
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 6)
        TestUtils.assertEquals(arrayOf(
                7, 7, 1, 9, 2, 1,
                5,
                8, 3, 6, 4, 9, 8,
                2
        ), game)
    }

    @Test
    fun testMoveSkipOpponentBigPitVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 13)
        TestUtils.assertEquals(arrayOf(
                8, 3, 6, 4, 9, 8,
                2,
                7, 7, 1, 9, 2, 1,
                5
        ), game)
    }

    @Test
    fun testMoveLastStoneInOpponentsEmptyPitVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 5, 6,
                4
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 12) //  no capture
        TestUtils.assertEquals(arrayOf(
                6, 8, 1, 8, 6, 6,
                3,
                6, 6, 4, 6, 0, 7,
                5
        ), game)
    }

    @Test
    fun testMoveLastStoneInOpponentsEmptyPitVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 5, 6,
                3,
                5, 7, 0, 8, 6, 6,
                4
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 5) //  no capture
        TestUtils.assertEquals(arrayOf(
                6, 6, 4, 6, 0, 7,
                4,
                6, 8, 1, 8, 6, 6,
                4
        ), game)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerEmptyPitVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 1, 0,
                14
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 12) //  capture
        TestUtils.assertEquals(arrayOf(
                0, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 0, 1,
                19
        ), game)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerEmptyPitVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 5) //  capture
        TestUtils.assertEquals(arrayOf(
                6, 6, 4, 6, 0, 1,
                18,
                0, 7, 0, 8, 6, 6,
                4
        ), game)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerBigPit1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 7, 4, 9, 2, 6,
                8,
                5, 10, 4, 6, 3, 0,
                2
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 1)
        TestUtils.assertEquals(arrayOf(
                0, 8, 5, 10, 3, 7,
                9,
                5, 10, 4, 6, 3, 0,
                2
        ), game)
        Assertions.assertEquals(GameTurn.PLAYER1, game.gameTurn)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerBigPit2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 4, 9, 3, 6,
                8,
                6, 10, 4, 5, 3, 0,
                2
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 8)
        TestUtils.assertEquals(arrayOf(
                5, 7, 4, 9, 3, 6,
                8,
                0, 11, 5, 6, 4, 1,
                3
        ), game)
        Assertions.assertEquals(GameTurn.PLAYER2, game.gameTurn)
    }

    @Test
    fun testMovePlayer1GameFinishedEqual() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 1,
                35,
                0, 0, 0, 4, 0, 1,
                31
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 6)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                36,
                0, 0, 0, 0, 0, 0,
                36
        ), game)
        Assertions.assertEquals(GameFinishState.FINISHED_EQUAL, game.finishedState)
    }

    @Test
    fun testMovePlayer2GameFinishedEqual() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 2, 0, 2, 0, 0,
                32,
                0, 0, 0, 0, 0, 1,
                35), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 13)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                36,
                0, 0, 0, 0, 0, 0,
                36
        ), game)
        Assertions.assertEquals(GameFinishState.FINISHED_EQUAL, game.finishedState)
    }

    @Test
    fun testMovePlayer1GameFinishedPlayer1Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 1,
                40,
                0, 7, 0, 0, 2, 0,
                22), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 6)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                41,
                0, 0, 0, 0, 0, 0,
                31
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER1_WON, game.finishedState)
    }

    @Test
    fun testMovePlayer2GameFinishedPlayer1Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 3, 0, 2, 0,
                36,
                0, 0, 0, 0, 0, 1,
                30), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 13)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                41,
                0, 0, 0, 0, 0, 0,
                31
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER1_WON, game.finishedState)
    }

    @Test
    fun testMovePlayer1GameFinishedPlayer2Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 1,
                30,
                0, 7, 0, 0, 2, 0,
                32), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        mockFindGame(game)
        gameService.move(1L, 6)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                31,
                0, 0, 0, 0, 0, 0,
                41
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER2_WON, game.finishedState)
    }

    @Test
    fun testMovePlayer2GameFinishedPlayer2Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 3, 0, 2, 0,
                26,
                0, 0, 0, 0, 0, 1,
                40), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        mockFindGame(game)
        gameService.move(1L, 13)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                31,
                0, 0, 0, 0, 0, 0,
                41
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER2_WON, game.finishedState)
    }

    @Test
    fun testValidationForNullGameId() {
        assertThrows<NotAcceptableException> {
            gameService.move(null, 1) //  it must throw exception and not go further
        }
    }

    @Test
    fun testValidationForNullPitId() {
        assertThrows<NotAcceptableException> {
            gameService.move(1L, null) //  it must throw exception and not go further
        }
    }

    @Test
    fun testValidationForNullGameIdAndPitId() {
        assertThrows<NotAcceptableException> {
            gameService.move(null, null) //  it must throw exception and not go further
        }
    }

    @Test
    fun testValidationForGameIdNotFound() {
        mockFindEmptyGame()
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 1)
        }
    }

    @Test
    fun testValidationForSmallerPitId() {
        mockFindGame(GameFactory.createDefaultGame(6, 6))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 0)
        }
    }

    @Test
    fun testValidationForLargerPitId() {
        mockFindGame(GameFactory.createDefaultGame(6, 6))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 15)
        }
    }

    @Test
    fun testValidationForBigPit1() {
        mockFindGame(GameFactory.createDefaultGame(6, 6))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 7)
        }
    }

    @Test
    fun testValidationForBigPit2() {
        mockFindGame(GameFactory.createDefaultGame(6, 6))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 14)
        }
    }

    @Test
    fun testValidationForWrongGameTurn1() {
        mockFindGame(GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 9)
        }
    }

    @Test
    fun testValidationForWrongGameTurn2() {
        mockFindGame(GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 3)
        }
    }


    @Test
    fun testValidationForFinishedEqualGame() {
        mockFindGame(GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ), GameFinishState.FINISHED_EQUAL, GameTurn.PLAYER2))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 11)
        }
    }

    @Test
    fun testValidationForFinishedPlayer1WonGame() {
        mockFindGame(GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ), GameFinishState.PLAYER1_WON, GameTurn.PLAYER2))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 11)
        }
    }

    @Test
    fun testValidationForFinishedPlayer2WonGame() {
        mockFindGame(GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ), GameFinishState.PLAYER2_WON, GameTurn.PLAYER2))
        assertThrows<NotAcceptableException> {
            gameService.move(1L, 11)
        }
    }


    ////////////////    Mock

    private fun mockSaveGame() {
        val game = GameFactory.createDefaultGame(6, 6)
        //TODO HELP I don't like 'Game::class.java'. Is there another way of doing this?
        Mockito.`when`(gameRepository.save(Mockito.any(Game::class.java))).thenReturn(game)
    }

    private fun mockFindGame(game: Game) {
        Mockito.`when`(gameRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(game))
    }

    private fun mockFindEmptyGame() {
        Mockito.`when`(gameRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty())
    }

    ////////////////    End Mock
}
