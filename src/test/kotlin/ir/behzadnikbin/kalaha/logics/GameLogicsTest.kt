package ir.behzadnikbin.kalaha.logics

import ir.behzadnikbin.kalaha.dto.GameFinishState
import ir.behzadnikbin.kalaha.dto.GameMoveCommandDto
import ir.behzadnikbin.kalaha.dto.GameMoveCommandType
import ir.behzadnikbin.kalaha.dto.GameTurn
import ir.behzadnikbin.kalaha.utils.TestUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * Units tests for {@link GameLogics GameLogic}
 * All functions are static, so it doesn't need Spring context.
 * Most parts of this class is converted from Java to Kotlin by IntellijIDEA
 *
 * @author Behzad
 */
internal class GameLogicsTest {

    @Test
    fun testIsPlayer1BigPit() {
        for (i in 1..14) {
            val isBigPit = GameLogics.isPlayer1BigPit(i, 14)
            if (i == 7) {
                Assertions.assertTrue(isBigPit)
            } else {
                Assertions.assertFalse(isBigPit)
            }
        }
    }

    @Test
    fun testIsPlayer2BigPit() {
        for (i in 1..14) {
            val isBigPit = GameLogics.isPlayer2BigPit(i, 14)
            if (i == 14) {
                Assertions.assertTrue(isBigPit)
            } else {
                Assertions.assertFalse(isBigPit)
            }
        }
    }

    @Test
    fun testIsBigPitFor4Pits() {
        for (i in 1..10) {
            val isBigPit = GameLogics.isBigPit(i, 10)
            if (i == 5 || i == 10) {
                Assertions.assertTrue(isBigPit)
            } else {
                Assertions.assertFalse(isBigPit)
            }
        }
    }

    @Test
    fun testIsBigPitFor6Pits() {
        for (i in 1..14) {
            val isBigPit = GameLogics.isBigPit(i, 14)
            if (i == 7 || i == 14) {
                Assertions.assertTrue(isBigPit)
            } else {
                Assertions.assertFalse(isBigPit)
            }
        }
    }

    @Test
    fun testGetBigPitIds() {
        val bigPitIds14 = GameLogics.getBigPitIds(14)
        Assertions.assertEquals(7, bigPitIds14.first)
        Assertions.assertEquals(14, bigPitIds14.second)
        val bigPitIds16 = GameLogics.getBigPitIds(16)
        Assertions.assertEquals(8, bigPitIds16.first)
        Assertions.assertEquals(16, bigPitIds16.second)
    }

    @Test
    fun testGetPitsCountFromAllPitsCount() {
        Assertions.assertEquals(6, GameLogics.getPitsCountFromTotalPitsCount(14))
        Assertions.assertEquals(7, GameLogics.getPitsCountFromTotalPitsCount(16))
    }

    @Test
    fun testGetTotalPitsCountFromPitsCount() {
        Assertions.assertEquals(14, GameLogics.getTotalPitsCountFromPitsCount(6))
        Assertions.assertEquals(18, GameLogics.getTotalPitsCountFromPitsCount(8))
    }

    @Test
    fun testGetNextPitIdExcludingOpponentBigPit() {
        Assertions.assertEquals(2, GameLogics.getNextPitIdExcludingOpponentBigPit(1, 14, 14))
        Assertions.assertEquals(3, GameLogics.getNextPitIdExcludingOpponentBigPit(2, 14, 14))
        Assertions.assertEquals(4, GameLogics.getNextPitIdExcludingOpponentBigPit(3, 14, 14))
        Assertions.assertEquals(5, GameLogics.getNextPitIdExcludingOpponentBigPit(4, 14, 14))
        Assertions.assertEquals(6, GameLogics.getNextPitIdExcludingOpponentBigPit(5, 14, 14))
        Assertions.assertEquals(7, GameLogics.getNextPitIdExcludingOpponentBigPit(6, 14, 14))

        //  result for 7 is ignored but it must not throw exception
        GameLogics.getNextPitIdExcludingOpponentBigPit(7, 7, 14)
        Assertions.assertEquals(9, GameLogics.getNextPitIdExcludingOpponentBigPit(8, 14, 14))
        Assertions.assertEquals(10, GameLogics.getNextPitIdExcludingOpponentBigPit(9, 14, 14))
        Assertions.assertEquals(11, GameLogics.getNextPitIdExcludingOpponentBigPit(10, 14, 14))
        Assertions.assertEquals(12, GameLogics.getNextPitIdExcludingOpponentBigPit(11, 14, 14))
        Assertions.assertEquals(13, GameLogics.getNextPitIdExcludingOpponentBigPit(12, 14, 14))
        Assertions.assertEquals(1, GameLogics.getNextPitIdExcludingOpponentBigPit(13, 14, 14)) //  skip opponent's big pit

        //  result for 14 is ignored but it must not throw exception
        GameLogics.getNextPitIdExcludingOpponentBigPit(14, 7, 14)
        Assertions.assertEquals(2, GameLogics.getNextPitIdExcludingOpponentBigPit(1, 7, 14))
        Assertions.assertEquals(3, GameLogics.getNextPitIdExcludingOpponentBigPit(2, 7, 14))
        Assertions.assertEquals(4, GameLogics.getNextPitIdExcludingOpponentBigPit(3, 7, 14))
        Assertions.assertEquals(5, GameLogics.getNextPitIdExcludingOpponentBigPit(4, 7, 14))
        Assertions.assertEquals(6, GameLogics.getNextPitIdExcludingOpponentBigPit(5, 7, 14))
        Assertions.assertEquals(8, GameLogics.getNextPitIdExcludingOpponentBigPit(6, 7, 14)) //  skip opponent's big pit

        //  result for 7 is ignored but it must not throw exception
        GameLogics.getNextPitIdExcludingOpponentBigPit(7, 7, 14)
        Assertions.assertEquals(9, GameLogics.getNextPitIdExcludingOpponentBigPit(8, 7, 14))
        Assertions.assertEquals(10, GameLogics.getNextPitIdExcludingOpponentBigPit(9, 7, 14))
        Assertions.assertEquals(11, GameLogics.getNextPitIdExcludingOpponentBigPit(10, 7, 14))
        Assertions.assertEquals(12, GameLogics.getNextPitIdExcludingOpponentBigPit(11, 7, 14))
        Assertions.assertEquals(13, GameLogics.getNextPitIdExcludingOpponentBigPit(12, 7, 14))
        Assertions.assertEquals(14, GameLogics.getNextPitIdExcludingOpponentBigPit(13, 7, 14))

        //  result for 14 is ignored but it must not throw exception
        GameLogics.getNextPitIdExcludingOpponentBigPit(14, 7, 14)
    }

    @Test
    fun testGetPlayerNumberByPitId() {
        Assertions.assertEquals(GameTurn.PLAYER1, GameLogics.getPlayerNumberByPitId(1, 14))
        Assertions.assertEquals(GameTurn.PLAYER1, GameLogics.getPlayerNumberByPitId(3, 14))
        Assertions.assertEquals(GameTurn.PLAYER1, GameLogics.getPlayerNumberByPitId(6, 14))
        Assertions.assertEquals(GameTurn.PLAYER1, GameLogics.getPlayerNumberByPitId(7, 14))
        Assertions.assertEquals(GameTurn.PLAYER2, GameLogics.getPlayerNumberByPitId(8, 14))
        Assertions.assertEquals(GameTurn.PLAYER2, GameLogics.getPlayerNumberByPitId(10, 14))
        Assertions.assertEquals(GameTurn.PLAYER2, GameLogics.getPlayerNumberByPitId(13, 14))
        Assertions.assertEquals(GameTurn.PLAYER2, GameLogics.getPlayerNumberByPitId(14, 14))
    }

    @Test
    fun testGetOpponenBigPittIdOfSelectedPitId() {
        for (i in 1..7) {
            Assertions.assertEquals(14, GameLogics.getBigPitIdOfOpponent(i, 14))
        }
        for (i in 8..14) {
            Assertions.assertEquals(7, GameLogics.getBigPitIdOfOpponent(i, 14))
        }
    }

    @Test
    fun testGetCurrentPlayerBigPitIdOfSelectedPitId() {
        for (i in 1..7) {
            Assertions.assertEquals(7, GameLogics.getBigPitIdOfCurrentPlayer(i, 14))
        }
        for (i in 8..14) {
            Assertions.assertEquals(14, GameLogics.getBigPitIdOfCurrentPlayer(i, 14))
        }
    }

    @Test
    fun testGetMirroredPitId() {
        Assertions.assertEquals(13, GameLogics.getMirroredPitId(1, 14))
        Assertions.assertEquals(11, GameLogics.getMirroredPitId(3, 14))
        Assertions.assertEquals(8, GameLogics.getMirroredPitId(6, 14))
        Assertions.assertEquals(6, GameLogics.getMirroredPitId(8, 14))
        Assertions.assertEquals(4, GameLogics.getMirroredPitId(10, 14))
        Assertions.assertEquals(1, GameLogics.getMirroredPitId(13, 14))
    }

    @Test
    fun testIsGameFinishedFalse() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 7, 7, 7, 7, 7,
                1,
                6, 6, 6, 6, 6, 6,
                0
        ))
        Assertions.assertFalse(GameLogics.isGameFinished(game))
    }

    @Test
    fun testIsGameFinishedPlayer1SideEmpty() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 0,
                23,
                10, 16, 0, 0, 6, 1,
                16
        ))
        Assertions.assertTrue(GameLogics.isGameFinished(game))
    }

    @Test
    fun testIsGameFinishedPlayer2SideEmpty() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                10, 16, 0, 0, 6, 1,
                16,
                0, 0, 0, 0, 0, 0,
                23))
        Assertions.assertTrue(GameLogics.isGameFinished(game))
    }

    @Test
    fun testIsGameFinishedFalse1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 1, 0, 1, 0,
                30,
                0, 0, 1, 0, 0, 0,
                39
        ))
        Assertions.assertFalse(GameLogics.isGameFinished(game))
    }

    @Test
    fun testIsGameFinishedFalse2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 1,
                30,
                1, 0, 0, 0, 0, 0,
                40
        ))
        Assertions.assertFalse(GameLogics.isGameFinished(game))
    }

    @Test
    fun testIsGameFinishedFalse3() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                1, 0, 0, 0, 0, 0,
                30,
                0, 0, 0, 0, 0, 1,
                40
        ))
        Assertions.assertFalse(GameLogics.isGameFinished(game))
    }

    @Test
    fun testPutEveryRemainedStoneInBigPit1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                10, 16, 0, 0, 6, 1,
                16,
                0, 0, 0, 0, 0, 0,
                23))
        GameLogics.putEveryRemainedStoneInBigPit(game)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                49,
                0, 0, 0, 0, 0, 0,
                23), game)
    }

    @Test
    fun testPutEveryRemainedStoneInBigPit2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 0,
                23,
                10, 16, 0, 0, 6, 1,
                16
        ))
        GameLogics.putEveryRemainedStoneInBigPit(game)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                23,
                0, 0, 0, 0, 0, 0,
                49
        ), game)
    }

    @Test
    fun testGetGameFinishStateEquals() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 0,
                36,
                0, 0, 0, 0, 0, 0,
                36
        ))
        Assertions.assertEquals(GameFinishState.FINISHED_EQUAL, GameLogics.getGameFinishState(game))
    }

    @Test
    fun testGetGameFinishStatePlayer1Wins() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 0,
                40,
                0, 0, 0, 0, 0, 0,
                32
        ))
        Assertions.assertEquals(GameFinishState.PLAYER1_WON, GameLogics.getGameFinishState(game))
    }

    @Test
    fun testGetGameFinishStatePlayer2Wins() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 0,
                20,
                0, 0, 0, 0, 0, 0,
                52
        ))
        Assertions.assertEquals(GameFinishState.PLAYER2_WON, GameLogics.getGameFinishState(game))
    }

    @Test
    fun testMovePlayer1NormalGameTurn() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        GameLogics.move(game, 2)
        Assertions.assertEquals(GameTurn.PLAYER2, game.gameTurn)
    }

    @Test
    fun testMovePlayer2NormalGameTurn() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        GameLogics.move(game, 8)
        Assertions.assertEquals(GameTurn.PLAYER1, game.gameTurn)
    }

    @Test
    fun testMovePlayer1GameTurnForLastStoneInBigPit() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        GameLogics.move(game, 3)
        Assertions.assertEquals(GameTurn.PLAYER1, game.gameTurn)
    }

    @Test
    fun testMovePlayer2eGameTurnForLastStoneInBigPit() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                6, 5, 0, 8, 1, 21,
                3
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        GameLogics.move(game, 8)
        Assertions.assertEquals(GameTurn.PLAYER2, game.gameTurn)
    }

    @Test
    fun testMoveCreatedGameVer1() {
        val game = GameFactory.createDefaultGame(6, 6)
        GameLogics.move(game, 1)
        TestUtils.assertEquals(arrayOf(
                0, 7, 7, 7, 7, 7,
                1,
                6, 6, 6, 6, 6, 6,
                0
        ), game)
    }

    @Test
    fun testMoveCreatedGameVer2() {
        val game = GameFactory.createDefaultGame(6, 6)
        GameLogics.move(game, 9)
        TestUtils.assertEquals(arrayOf(
                7, 6, 6, 6, 6, 6,
                0,
                6, 0, 7, 7, 7, 7,
                1
        ), game)
    }


    @Test
    fun testMoveMiddleOfGameVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                7, 7, 0, 8, 9, 4,
                0,
                4, 2, 10, 0, 6, 5,
                10
        ))
        GameLogics.move(game, 8)
        TestUtils.assertEquals(arrayOf(
                7, 7, 0, 8, 9, 4,
                0,
                0, 3, 11, 1, 7, 5,
                10
        ), game)
    }

    @Test
    fun testMoveMiddleOfGameVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 7, 6,
                2
        ))
        GameLogics.move(game, 12)
        TestUtils.assertEquals(arrayOf(
                6, 8, 1, 9, 7, 6,
                3,
                6, 6, 4, 6, 0, 7,
                3
        ), game)
    }

    @Test
    fun testMoveSkipOpponentBigPitVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 6, 0, 8, 1, 21,
                3,
                6, 1, 4, 2, 7, 6,
                2
        ))
        GameLogics.move(game, 6)
        TestUtils.assertEquals(arrayOf(
                7, 7, 1, 9, 2, 1,
                5,
                8, 3, 6, 4, 9, 8,
                2
        ), game)
    }

    @Test
    fun testMoveSkipOpponentBigPitVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 1, 4, 2, 7, 6,
                2,
                5, 6, 0, 8, 1, 21,
                3
        ))
        GameLogics.move(game, 13)
        TestUtils.assertEquals(arrayOf(
                8, 3, 6, 4, 9, 8,
                2,
                7, 7, 1, 9, 2, 1,
                5
        ), game)
    }

    @Test
    fun testMoveLastStoneInOpponentsEmptyPitVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 5, 6,
                4
        ))
        GameLogics.move(game, 12) //  no capture
        TestUtils.assertEquals(arrayOf(
                6, 8, 1, 8, 6, 6,
                3,
                6, 6, 4, 6, 0, 7,
                5
        ), game)
    }

    @Test
    fun testMoveLastStoneInOpponentsEmptyPitVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 5, 6,
                3,
                5, 7, 0, 8, 6, 6,
                4
        ))
        GameLogics.move(game, 5) //  no capture
        TestUtils.assertEquals(arrayOf(
                6, 6, 4, 6, 0, 7,
                4,
                6, 8, 1, 8, 6, 6,
                4
        ), game)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerEmptyPitVer1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 1, 0,
                14
        ))
        GameLogics.move(game, 12) //  capture
        TestUtils.assertEquals(arrayOf(
                0, 7, 0, 8, 6, 6,
                3,
                6, 6, 4, 6, 0, 1,
                19
        ), game)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerEmptyPitVer2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ))
        GameLogics.move(game, 5) //  capture
        TestUtils.assertEquals(arrayOf(
                6, 6, 4, 6, 0, 1,
                18,
                0, 7, 0, 8, 6, 6,
                4
        ), game)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerBigPit1() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 7, 4, 9, 2, 6,
                8,
                5, 10, 4, 6, 3, 0,
                2
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        GameLogics.move(game, 1)
        TestUtils.assertEquals(arrayOf(
                0, 8, 5, 10, 3, 7,
                9,
                5, 10, 4, 6, 3, 0,
                2
        ), game)
        Assertions.assertEquals(GameTurn.PLAYER1, game.gameTurn)
    }

    @Test
    fun testMoveLastStoneInCurrentPlayerBigPit2() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                5, 7, 4, 9, 3, 6,
                8,
                6, 10, 4, 5, 3, 0,
                2
        ), GameFinishState.NOT_FINISHED, GameTurn.PLAYER2)
        GameLogics.move(game, 8)
        TestUtils.assertEquals(arrayOf(
                5, 7, 4, 9, 3, 6,
                8,
                0, 11, 5, 6, 4, 1,
                3
        ), game)
        Assertions.assertEquals(GameTurn.PLAYER2, game.gameTurn)
    }

    @Test
    fun testMovePlayer1GameFinishedEqual() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 1,
                35,
                0, 0, 0, 4, 0, 1,
                31
        ))
        GameLogics.move(game, 6)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                36,
                0, 0, 0, 0, 0, 0,
                36
        ), game)
        Assertions.assertEquals(GameFinishState.FINISHED_EQUAL, game.finishedState)
    }

    @Test
    fun testMovePlayer2GameFinishedEqual() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 2, 0, 2, 0, 0,
                32,
                0, 0, 0, 0, 0, 1,
                35))
        GameLogics.move(game, 13)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                36,
                0, 0, 0, 0, 0, 0,
                36
        ), game)
        Assertions.assertEquals(GameFinishState.FINISHED_EQUAL, game.finishedState)
    }

    @Test
    fun testMovePlayer1GameFinishedPlayer1Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 1,
                40,
                0, 7, 0, 0, 2, 0,
                22))
        GameLogics.move(game, 6)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                41,
                0, 0, 0, 0, 0, 0,
                31
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER1_WON, game.finishedState)
    }

    @Test
    fun testMovePlayer2GameFinishedPlayer1Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 3, 0, 2, 0,
                36,
                0, 0, 0, 0, 0, 1,
                30))
        GameLogics.move(game, 13)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                41,
                0, 0, 0, 0, 0, 0,
                31
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER1_WON, game.finishedState)
    }

    @Test
    fun testMovePlayer1GameFinishedPlayer2Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 0, 0, 0, 1,
                30,
                0, 7, 0, 0, 2, 0,
                32))
        GameLogics.move(game, 6)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                31,
                0, 0, 0, 0, 0, 0,
                41
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER2_WON, game.finishedState)
    }

    @Test
    fun testMovePlayer2GameFinishedPlayer2Won() {
        val game = GameFactory.createGameWithCustomState(arrayOf(
                0, 0, 3, 0, 2, 0,
                26,
                0, 0, 0, 0, 0, 1,
                40))
        GameLogics.move(game, 13)
        TestUtils.assertEquals(arrayOf(
                0, 0, 0, 0, 0, 0,
                31,
                0, 0, 0, 0, 0, 0,
                41
        ), game)
        Assertions.assertEquals(GameFinishState.PLAYER2_WON, game.finishedState)
    }

    @Test
    fun testMoveCommands1() {
        //  capture state will cover all move command types
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 0,
                13,
                5, 7, 0, 8, 6, 6,
                4
        ))
        GameLogics.move(game, 5) //  capture
        val moveCommands = game.moveCommands
        Assertions.assertNotNull(moveCommands)
        Assertions.assertEquals(2, moveCommands.size)
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 5, 6), moveCommands[0])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.CAPTURE, 8, 7), moveCommands[1])
    }

    @Test
    fun testMoveCommands2() {
        //  capture state will cover all move command types
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 4, 6, 1, 4,
                13,
                5, 3, 6, 8, 0, 6,
                4
        ))
        GameLogics.move(game, 9) //  capture
        val moveCommands = game.moveCommands
        Assertions.assertNotNull(moveCommands)
        Assertions.assertEquals(4, moveCommands.size)
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 9, 10), moveCommands[0])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 9, 11), moveCommands[1])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 9, 12), moveCommands[2])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.CAPTURE, 2, 14), moveCommands[3])
    }

    @Test
    fun testMoveCommands3() {
        //  capture state will cover all move command types
        val game = GameFactory.createGameWithCustomState(arrayOf(
                6, 6, 6, 6, 6, 6,
                0,
                6, 6, 6, 6, 6, 6,
                0
        ))
        GameLogics.move(game, 1) //  not capture
        val moveCommands = game.moveCommands
        Assertions.assertNotNull(moveCommands)
        Assertions.assertEquals(6, moveCommands.size)
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 1, 2), moveCommands[0])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 1, 3), moveCommands[1])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 1, 4), moveCommands[2])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 1, 5), moveCommands[3])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 1, 6), moveCommands[4])
        Assertions.assertEquals(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, 1, 7), moveCommands[5])
    }
}
