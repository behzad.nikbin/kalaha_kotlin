package ir.behzadnikbin.kalaha

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest


/**
 * Test for context load
 *
 * @author Behzad
 */
@SpringBootTest
class KalahaApplicationTests {

    @Test
    fun contextLoads() {

    }

}
