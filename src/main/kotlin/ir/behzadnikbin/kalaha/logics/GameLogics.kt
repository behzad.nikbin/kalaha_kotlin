package ir.behzadnikbin.kalaha.logics

import ir.behzadnikbin.kalaha.data.model.Game
import ir.behzadnikbin.kalaha.dto.GameFinishState
import ir.behzadnikbin.kalaha.dto.GameMoveCommandDto
import ir.behzadnikbin.kalaha.dto.GameMoveCommandType
import ir.behzadnikbin.kalaha.dto.GameTurn
import ir.behzadnikbin.kalaha.utils.NotAcceptableException

/**
 * The main logics of kalaha game
 * All pitIds must be validated before calling these functions.
 *
 * @author Behzad
 */
//TODO HELP: is this the correct way of making a private constructor?
class GameLogics private constructor() {

    //TODO HELP: is this the correct way of defining a static functions in a class? I know I can make it either a Spring bean or an object class (singleton), but I prefer to learn static functions in a class as well.
    companion object {

        /**
         * Checks if the pitId is assigned to a big pit
         *
         * @param pitId          Id of a pit
         * @param totalPitsCount Total count of pits including big pits
         * @return boolean
         */
        fun isBigPit(pitId: Int, totalPitsCount: Int): Boolean {
            return isPlayer1BigPit(pitId, totalPitsCount) || isPlayer2BigPit(pitId, totalPitsCount)
        }

        /**
         * Checks if the pitId is assigned to the big pit of player1
         *
         * @param pitId          Id of a pit
         * @param totalPitsCount Total count of pits including big pits
         * @return boolean
         */
        fun isPlayer1BigPit(pitId: Int, totalPitsCount: Int): Boolean {
            return pitId == totalPitsCount / 2
        }

        /**
         * Checks if the pitId is assigned to the big pit of player2
         *
         * @param pitId          Id of a pit
         * @param totalPitsCount Total count of pits including big pits
         * @return boolean
         */
        fun isPlayer2BigPit(pitId: Int, totalPitsCount: Int): Boolean {
            return pitId == totalPitsCount
        }

        /**
         * Get id of big pits according to the given pits count
         *
         * @param totalPitsCount Total count of pits including big pits
         * @return {@link Pair Pair} of big pit id of player1 and big pit id of player2
         */
        fun getBigPitIds(totalPitsCount: Int): Pair<Int, Int> {
            return Pair(totalPitsCount / 2, totalPitsCount)
        }

        /**
         * Calculates pits count per player (excluding big pits) from the given pits count
         *
         * @param totalPitsCount Total count of pits including big pits
         * @return int (pits count)
         */
        fun getPitsCountFromTotalPitsCount(totalPitsCount: Int): Int {
            return (totalPitsCount - 2) / 2
        }

        /**
         * Calculates total pits count (including big pits) from the given pits count
         *
         * @param pitsCount Count of pits per player (excluding big pits)
         * @return int (total number of pits)
         */
        fun getTotalPitsCountFromPitsCount(pitsCount: Int): Int {
            return 2 * pitsCount + 2
        }

        /**
         * Get next pitId to move stone (next pit excluding opponent's big pit). Used in move function.
         *
         * @param pitId            Id of pit
         * @param opponentBigPitId Id of opponent's big pit
         * @param totalPitsCount   Total count of pits including big pits
         * @return Id of next possible pit
         */
        fun getNextPitIdExcludingOpponentBigPit(pitId: Int, opponentBigPitId: Int, totalPitsCount: Int): Int {
            var nextPitId = pitId + 1
            if (nextPitId == opponentBigPitId) {
                nextPitId++
            }
            if (nextPitId > totalPitsCount) {
                nextPitId -= totalPitsCount
            }
            return nextPitId
        }

        /**
         * Get player number (Player1/Player2) according to  the given pitId (which is the id of selected pit) and totalPitsCount
         *
         * @param pitId          Id of pit
         * @param totalPitsCount Total count of pits including big pits
         * @return Player1/Player2
         */
        fun getPlayerNumberByPitId(pitId: Int, totalPitsCount: Int): GameTurn {
            return if (pitId <= totalPitsCount / 2)
                GameTurn.PLAYER1
            else
                GameTurn.PLAYER2
        }

        /**
         * Get pitId of opponent's big pit according to the given pitId (which is the id of selected pit) and totalPitsCount
         *
         * @param pitId          Id of selected pit
         * @param totalPitsCount Total count of holes including big pits
         * @return pitId of big pit related to the selected pitId
         */
        fun getBigPitIdOfOpponent(pitId: Int, totalPitsCount: Int): Int {
            return if (pitId > totalPitsCount / 2)
                totalPitsCount / 2        //  player1's big pit id
            else
                totalPitsCount             //  player2's big pit id
        }

        /**
         * Get pitId of current player's big pit according to the given pitId (which is the id of selected pit) and totalPitsCount
         *
         * @param pitId          Id of selected pit
         * @param totalPitsCount Total count of holes including big pits
         * @return pitId of big pit related to the selected pitId
         */
        fun getBigPitIdOfCurrentPlayer(pitId: Int, totalPitsCount: Int): Int {
            return if (pitId <= totalPitsCount / 2)
                totalPitsCount / 2        //  player1's big pit id
            else
                totalPitsCount            //  player2's big pit id
        }

        /**
         * Get id of the pit in front of the given pitId.
         * This function does <b>not</b> support <b>big pits</b>
         *
         * @param pitId          Id of pit
         * @param totalPitsCount Total count of pits including big pits
         * @return pitId of the pit in front of the given pit
         */
        fun getMirroredPitId(pitId: Int, totalPitsCount: Int): Int {
            return totalPitsCount - pitId
        }

        /**
         * The game is finished, if all pits of a player are empty.
         *
         * @param game {@link Game Game} object as input
         * @return boolean (true: if the game is finished)
         */
        fun isGameFinished(game: Game): Boolean {
            val pits = game.pits
            val totalPitsCount = pits.size
            val bigPitIds = getBigPitIds(totalPitsCount)
            val pitsEmpty1 = IntRange(1, bigPitIds.first - 1).all { pits[it]?.stoneCount == 0 }
            val pitsEmpty2 = IntRange(bigPitIds.first + 1, bigPitIds.second - 1).all { pits[it]?.stoneCount == 0 }
            return pitsEmpty1 || pitsEmpty2
        }

        /**
         * This function puts every remained stone to big pit.
         * It must be called after the game is finished (one side is empty)
         *
         * @param game {@link Game Game} object
         */
        fun putEveryRemainedStoneInBigPit(game: Game) {
            val pits = game.pits
            val totalPitsCount = pits.size
            for (pit in pits.values) {
                //  find big pit related to this pit
                val bigPitId = getBigPitIdOfCurrentPlayer(pit.pitId, totalPitsCount)
                //  put every stone of pit to big pit
                val stoneCount = pit.stoneCount
                pit.stoneCount = 0
                val bigPit = pits[bigPitId]
                if (bigPit != null) {
                    bigPit.stoneCount += stoneCount
                }
            }
        }

        /**
         * This function compares big pit stones and returns the game state (the winner)
         * It doesn't make any change on the input game.
         *
         * @param game {@link Game Game} object
         * @return {@link GameFinishState GameFinishState} is finish state of the game (the winner)
         */
        fun getGameFinishState(game: Game): GameFinishState {
            val pits = game.pits
            //  find big pit ids
            val bigPitIds = getBigPitIds(pits.size)
            val player1StoneCount = pits[bigPitIds.first]?.stoneCount ?: 0
            val player2StoneCount = pits[bigPitIds.second]?.stoneCount ?: 0
            //  compare stone counts
            if (player1StoneCount == player2StoneCount) {
                return GameFinishState.FINISHED_EQUAL;
            } else if (player1StoneCount > player2StoneCount) {
                return GameFinishState.PLAYER1_WON;
            } else {
                return GameFinishState.PLAYER2_WON;
            }
        }

        /**
         * The main action of the game (move)
         * {@link Game Game} object and pitId must be validated before calling this function
         *
         * @param game  {@link Game Game} object which is used as both input and output
         * @param pitId Id of {@link Pit Pit} that player wants to move its stones
         */
        fun move(game: Game, pitId: Int) {
            val moveCommands = ArrayList<GameMoveCommandDto>()
            game.moveCommands = moveCommands
            val pits = game.pits
            val totalPitsCount = pits.size
            //  find opponent's bit pit id from selected pitId, used in finding next pit id latter on
            val opponentBigPitId = getBigPitIdOfOpponent(pitId, totalPitsCount)
            val currentPlayerBigPitId = getBigPitIdOfCurrentPlayer(pitId, totalPitsCount)

            val selectedPit = pits[pitId] ?: throw NotAcceptableException()
            val selectedPitStoneCount = selectedPit.stoneCount
            selectedPit.stoneCount = 0          //  make selectedPit empty

            var lastAffectedPitId = pitId
            //  add one stone to N next pits excluding opponent's big pit (N = selectedPitStoneCount)
            for (i in 1..selectedPitStoneCount) {
                val affectedPitId = getNextPitIdExcludingOpponentBigPit(lastAffectedPitId, opponentBigPitId, totalPitsCount)
                val pit = pits[affectedPitId] ?: continue

                //  keep capture command to add to moveCommands later
                var captureCommand: GameMoveCommandDto? = null
                if (i == selectedPitStoneCount) {           //  last stone

                    //  check for empty non big pit rule for the last stone
                    if (pit.stoneCount == 0) {
                        val gameTurn = getPlayerNumberByPitId(pitId, totalPitsCount)
                        val ownerOfAffectedPit = getPlayerNumberByPitId(affectedPitId, totalPitsCount)
                        //  the player must be owner of the affected pit to capture its stones
                        if (gameTurn == ownerOfAffectedPit) {
                            //  put every stone in the pit in front of the affectedPitId to current player's big pit
                            val opponentPitId = getMirroredPitId(affectedPitId, totalPitsCount)
                            if (!isBigPit(opponentPitId, totalPitsCount)) {
                                val mirroredPit = pits[opponentPitId] ?: continue
                                val mirroredPitStonesCount = mirroredPit.stoneCount
                                mirroredPit.stoneCount = 0
                                val currentPlayerBigPit = pits[currentPlayerBigPitId] ?: continue
                                currentPlayerBigPit.stoneCount += mirroredPitStonesCount
                                captureCommand = GameMoveCommandDto(GameMoveCommandType.CAPTURE, opponentPitId, currentPlayerBigPitId)

                            }
                        }
                    }

                    //  check for the rule if the last stone is in the player's big pit
                    if (affectedPitId != currentPlayerBigPitId) {        //  last stone is not the player's big pit
                        val newTurn = game.gameTurn.getOpponent()
                        game.gameTurn = newTurn     //  change game turn
                    }

                }   //  end last stone

                //  add 1 to affected pit
                pit.stoneCount++
                moveCommands.add(GameMoveCommandDto(GameMoveCommandType.MOVE_ONE, pitId, affectedPitId))
                if (captureCommand != null) {
                    moveCommands.add(captureCommand)
                }
                lastAffectedPitId = affectedPitId
            }   //  end for

            //  check if the game is finished after the move. all move operations must be finished before this.
            if (isGameFinished(game)) {
                //  move every remained stone to big pit
                putEveryRemainedStoneInBigPit(game);
                //  game finish state
                game.finishedState = getGameFinishState(game)
            }
        }
        //  end move

    }

}