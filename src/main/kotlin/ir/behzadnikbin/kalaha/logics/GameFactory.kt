package ir.behzadnikbin.kalaha.logics

import ir.behzadnikbin.kalaha.data.model.Game
import ir.behzadnikbin.kalaha.data.model.Pit
import ir.behzadnikbin.kalaha.dto.GameFinishState
import ir.behzadnikbin.kalaha.dto.GameTurn
import ir.behzadnikbin.kalaha.utils.NotAcceptableException

/**
 * Create {@link Game Game} with given configurations
 *
 * @author Behzad
 */
class GameFactory private constructor() {

    companion object {

        /**
         * Create a default {@link Game Game}
         * Nothing is persisted here, just the game object is returned.
         *
         * @param pitsCount   Number of pits in one side
         * @param stonesCount Number of stones in each pits (except big pits)
         * @return {@link Game Game} object
         */
        fun createDefaultGame(pitsCount: Int, stonesCount: Int): Game {
            val game = Game()
            val totalPitsCount = GameLogics.getTotalPitsCountFromPitsCount(pitsCount)
            //  from 1 to totalPitsCount
            val map = HashMap<Int, Pit>()
            for (pitId in 1..totalPitsCount) {
                val currentPitStonesCount = if (pitId == totalPitsCount / 2 || pitId == totalPitsCount) 0 else stonesCount
                val pit = Pit(game, pitId, currentPitStonesCount)
                map[pitId] = pit
            }
            game.pits = map
            game.finishedState = GameFinishState.NOT_FINISHED
            game.gameTurn = GameTurn.PLAYER1
            return game
        }

        /**
         * Create a default {@link Game Game}
         * Nothing is persisted here, just the game object is returned.
         * Other game fields are ignored.
         *
         * @param gameStateArr An int array which holds number of stones in each pits
         * @return {@link Game Game} object
         */
        fun createGameWithCustomState(gameStateArr: Array<Int>): Game {
            return createGameWithCustomState(gameStateArr, GameFinishState.NOT_FINISHED, GameTurn.PLAYER1)
        }

        /**
         * Create a default {@link Game Game}
         * Nothing is persisted here, just the game object is returned.
         *
         * @param gameStateArr    An int array which holds number of stones in each pits
         * @param gameFinishState State of game
         * @param gameTurn        player turn
         * @return {@link Game Game} object
         */
        fun createGameWithCustomState(gameStateArr: Array<Int>, gameFinishState: GameFinishState, gameTurn: GameTurn): Game {
            //  array length must be an even number and greater or equal than 4 (2 big pits and at least 1 pit for every player)
            val totalPitsCount = gameStateArr.size
            if (totalPitsCount < 4 || totalPitsCount % 2 != 0) {
                throw NotAcceptableException()
            }
            val pitsCount: Int = GameLogics.getPitsCountFromTotalPitsCount(totalPitsCount)
            val totalStonesCount: Int = gameStateArr.sum()
            //  total stones count must be multiple of pits count
            if (totalStonesCount % pitsCount != 0) {
                throw NotAcceptableException()
            }
            val game = Game()
            game.gameTurn = gameTurn
            game.finishedState = gameFinishState
            val pits = HashMap<Int, Pit>()
            gameStateArr.forEachIndexed { index, stonesCount -> pits[index + 1] = Pit(game, index + 1, stonesCount) }
            game.pits = pits
            return game
        }

    }
}