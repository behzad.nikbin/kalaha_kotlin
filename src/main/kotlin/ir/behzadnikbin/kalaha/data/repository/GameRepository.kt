package ir.behzadnikbin.kalaha.data.repository

import ir.behzadnikbin.kalaha.data.model.Game
import org.springframework.stereotype.Repository

/**
 * {@link Repository Repository} of {@link Game Game}
 *
 * @author Behzad
 */
@Repository
interface GameRepository : AbstractRepository<Game, Long> {
}