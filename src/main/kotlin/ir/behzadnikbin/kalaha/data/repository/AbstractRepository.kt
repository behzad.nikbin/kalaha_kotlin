package ir.behzadnikbin.kalaha.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.NoRepositoryBean
import java.io.Serializable

/**
 * The parent of each {@link org.springframework.data.repository.Repository Repository}
 *
 * @param <T>  Generic type of entity
 * @param <ID> Generic type of id
 * @author Behzad
 */
@NoRepositoryBean
interface AbstractRepository<T, ID : Serializable> : JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
}