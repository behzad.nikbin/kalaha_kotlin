package ir.behzadnikbin.kalaha.data.model

import javax.persistence.*

/**
 * Pit entity which holds the pitId and stonesCount
 *
 * @author Behzad
 */
@Entity
@Table(name = "pit", indexes = [Index(name = "pitid_gameid_index", columnList = "game_id,pit_id", unique = true)])
class Pit() : AbstractModel() {

    constructor(game: Game, pitId: Int, stoneCount: Int) : this() {
        this.game = game
        this.pitId = pitId
        this.stoneCount = stoneCount
    }


    //  The game object which the pit belongs to
    @JoinColumn(name = "game_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)    //  lazy fetch to prevent unnecessary fetch of Game object
    var game: Game? = null

    //  Id of pit in the game
    @Column(nullable = false, name = "pit_id", length = 3)
    var pitId: Int = 0

    //  Stones count in the pit
    @Column(nullable = false, length = 2)
    var stoneCount: Int = 0

}