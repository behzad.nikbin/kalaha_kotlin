package ir.behzadnikbin.kalaha.data.model

import ir.behzadnikbin.kalaha.dto.GameFinishState
import ir.behzadnikbin.kalaha.dto.GameMoveCommandDto
import ir.behzadnikbin.kalaha.dto.GameTurn
import javax.persistence.*

/**
 * Game entity which holds the state of the game
 *
 * @author Behzad
 */
//TODO HELP: is this the correct way of defining a fields?
@Entity
class Game : AbstractModel() {

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 15)
    var finishedState: GameFinishState = GameFinishState.NOT_FINISHED

    //  By default, it's player1's turn
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 8)
    var gameTurn: GameTurn = GameTurn.PLAYER1

    //  pits are saved and loaded with the game object. data is converted to map automatically using ORM
    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "game")
    @MapKey(name = "pitId")
    var pits: Map<Int, Pit> = HashMap()

    //  moveCommands are not stored in DB, they are used to return movement steps in GameLogics
    @Transient
    var moveCommands: List<GameMoveCommandDto> = ArrayList()

}
