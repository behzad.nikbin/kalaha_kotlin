package ir.behzadnikbin.kalaha.data.model

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass

//TODO HELP: is this the correct way of defining a MappedSuperClass? defining as data class is not possible here so I also defined all of entity classes as non-data classes
@MappedSuperclass
abstract class AbstractModel {

    //TODO HELP: is this the correct way of defining an Id field?
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long? = null            //  UUID is also an option

}