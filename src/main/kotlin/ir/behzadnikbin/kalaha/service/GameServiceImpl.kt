package ir.behzadnikbin.kalaha.service

import ir.behzadnikbin.kalaha.data.model.Game
import ir.behzadnikbin.kalaha.data.repository.GameRepository
import ir.behzadnikbin.kalaha.dto.GameFinishState
import ir.behzadnikbin.kalaha.logics.GameFactory
import ir.behzadnikbin.kalaha.logics.GameLogics
import ir.behzadnikbin.kalaha.utils.NotAcceptableException
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import javax.transaction.Transactional

/**
 * Transactional service to serve game services using {@link GameLogics GameLogics} and persist to DB
 *
 * @author Behzad
 */
@Transactional
@Service
class GameServiceImpl(
        @Value("\${ir.behzadnikbin.kalaha.pits-count-per-player}") val PITS_COUNT_PER_PLAYER: Int,
        @Value("\${ir.behzadnikbin.kalaha.stones-count-per-pit}") val STONES_COUNT_PER_PIT: Int,
        private val gameRepository: GameRepository
) : GameService {


    override fun createGame(): Game {
        val game = GameFactory.createDefaultGame(PITS_COUNT_PER_PLAYER, STONES_COUNT_PER_PIT)
        return gameRepository.save(game)
    }

    override fun move(gameId: Long?, pitId: Int?): Game {
        //////////  validation of input params
        if (gameId == null || pitId == null) {
            throw NotAcceptableException()
        }
        //////////  end validation of input params

        //  load game from database
        val gameOpt = gameRepository.findById(gameId)
        if (!gameOpt.isPresent) {
            throw NotAcceptableException()
        }

        val game = gameOpt.get()
        //////////  validation according to loaded game

        //  game is already finished
        if (game.finishedState != GameFinishState.NOT_FINISHED) {
            throw NotAcceptableException()
        }

        val pits = game.pits
        val totalPitsCount = pits.size

        //  check if the loaded game contains the given pitId
        if (!pits.containsKey(pitId)) {
            throw NotAcceptableException()
        }

        //  pitId mustn't be a big pit
        if (GameLogics.isBigPit(pitId, totalPitsCount)) {
            throw NotAcceptableException()
        }

        //  check for user turn according to the given pitId
        val turn = GameLogics.getPlayerNumberByPitId(pitId, totalPitsCount)
        //  wrong game turn
        if (turn != game.gameTurn) {
            throw NotAcceptableException()
        }
        //////////  end validation according to loaded game

        //  move logic
        //////////  end validation according to loaded game

        //  move logic
        GameLogics.move(game, pitId)

        //  persist to DB

        //  persist to DB
        return gameRepository.save(game)

    }
}