package ir.behzadnikbin.kalaha.service

import ir.behzadnikbin.kalaha.data.model.Game

/**
 * Main service of the Kalaha game which do game actions. It is used in
 * {@link ir.behzadnikbin.kalaha.controller.GameController GameController}
 * to do game actions in user requests.
 *
 * @author Behzad
 */
interface GameService {
    /**
     * Creates a {@link Game Game} and persists it to database. Game id with be incremented automatically in database.
     *
     * @return {@link Game Game} including game id and game state.
     */
    fun createGame(): Game

    /**
     * Moves stones in the given pitId of the given gameId according to the game rules.
     * <p>
     *     <h3>Movement Rules:</h3>
     *     <ol>
     *         <li>
     *             The player who begins with the first move picks up all the stones in any of his
     *              own six pits, and sows the stones on to the right, one in each of the following
     *              pits, including his own big pit
     *         </li>
     *         <li>
     *             No stones are put in the opponents' big pit.
     *         </li>
     *         <li>
     *             If the  player's last stone lands in his own big pit, he gets another turn. This can be
     *              repeated several times before it's the other player's turn.
     *         </li>
     *         <li>
     *             During the game the pits are emptied on both sides. Always when the last stone
     *              lands in an own empty pit, the player captures his own stone and all stones in the
     *              opposite pit (the other player’s pit) and puts them in his own big pit.
     *         </li>
     *         <li>
     *             The game is over as soon as one of the sides runs out of stones. The player who
     *              still has stones in his pits keeps them and puts them in his big pit.
     *         </li>
     *         <li>
     *             The winner of the game is the player who has the most stones in his big pit.
     *         </li>
     *     </ol>
     * </p>
     *
     * @param gameId id of the game which user will play
     * @param pitId  id of pit, which user wants to move its stones
     * @return {@link Game Game} object
     */
    fun move(gameId: Long?, pitId: Int?): Game

}