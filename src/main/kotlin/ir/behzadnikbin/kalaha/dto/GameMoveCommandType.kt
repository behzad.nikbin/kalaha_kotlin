package ir.behzadnikbin.kalaha.dto

enum class GameMoveCommandType {
    MOVE_ONE,       //  move a stone from one pit to another
    CAPTURE,        //  capture all stones from one pit to a big pit
}
