package ir.behzadnikbin.kalaha.dto

enum class GameTurn {
    PLAYER1,
    PLAYER2,
    ;

    fun getOpponent(): GameTurn {
        return when (this) {
            PLAYER1 -> PLAYER2
            PLAYER2 -> PLAYER1
        }
    }
}