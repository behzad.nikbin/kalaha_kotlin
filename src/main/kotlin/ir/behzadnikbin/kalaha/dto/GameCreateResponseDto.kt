package ir.behzadnikbin.kalaha.dto

import ir.behzadnikbin.kalaha.data.model.Game

//TODO HELP: is this the correct way of defining a DTO? I wasn't able to make it a data class, because of the inheritance
open class GameCreateResponseDto(
        var gameId: Long?,
        var status: Map<Int, Int>,
        var gameTurn: GameTurn = GameTurn.PLAYER1,
        var gameFinishState: GameFinishState = GameFinishState.NOT_FINISHED) {

    constructor(game: Game) : this(
            game.id,
            game.pits.entries.map { it.key to it.value.stoneCount }.toMap(),
            game.gameTurn,
            game.finishedState
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GameCreateResponseDto

        if (gameId != other.gameId) return false

        return true
    }

    override fun hashCode(): Int {
        return gameId?.hashCode() ?: 0
    }

}
