package ir.behzadnikbin.kalaha.dto

import ir.behzadnikbin.kalaha.data.model.Game

class GameMoveResponseDto(
        gameId: Long?,
        status: Map<Int, Int>,
        gameTurn: GameTurn = GameTurn.PLAYER1,
        gameFinishState: GameFinishState = GameFinishState.NOT_FINISHED,
        val moveCommands: List<GameMoveCommandDto>) : GameCreateResponseDto(gameId, status, gameTurn, gameFinishState) {

    //TODO HELP: it seems that I can't call super's constructor here
    constructor(game: Game) : this(
            game.id,
            game.pits.entries.map { it.key to it.value.stoneCount }.toMap(),
            game.gameTurn,
            game.finishedState,
            game.moveCommands
    )

}
