package ir.behzadnikbin.kalaha.dto

//TODO HELP: is this the correct way of defining a DTO?
data class GameMoveCommandDto(
        val type: GameMoveCommandType,
        val from: Int,
        val to: Int
)
