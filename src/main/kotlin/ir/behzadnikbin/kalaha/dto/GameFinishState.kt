package ir.behzadnikbin.kalaha.dto

enum class GameFinishState {
    NOT_FINISHED,
    PLAYER1_WON,
    PLAYER2_WON,
    FINISHED_EQUAL,
}