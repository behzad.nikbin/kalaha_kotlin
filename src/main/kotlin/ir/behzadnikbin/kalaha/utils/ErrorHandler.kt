package ir.behzadnikbin.kalaha.utils

import com.fasterxml.jackson.core.JsonProcessingException
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

/**
 * This class handles {@link Exception exceptions} in controller package and return the appropriate response
 *
 * @author Behzad
 */
@Aspect
@Component
class ErrorHandler {

    /**
     * Captures any {@link Exception exceptions} in controller package
     *
     * @param pjp in passed by Spring Boot
     * @return {@link Any Any}
     */
    @Around("execution( * ir.behzadnikbin.kalaha.controller.*.*(..) )")
    fun catchExceptions(pjp: ProceedingJoinPoint): Any {
        return try {
            pjp.proceed()
        } catch (e: JsonProcessingException) {                  //  invalid json format in request
            ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build<Any>()
        } catch (e: NotAcceptableException) {                   //  invalid input
            ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build<Any>()
        } catch (e: Throwable) {                                //  other exceptions
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<Any>()
        }
    }

}