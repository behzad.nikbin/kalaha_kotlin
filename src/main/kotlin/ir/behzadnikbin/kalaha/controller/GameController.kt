package ir.behzadnikbin.kalaha.controller

import ir.behzadnikbin.kalaha.dto.GameCreateResponseDto
import ir.behzadnikbin.kalaha.dto.GameMoveResponseDto
import ir.behzadnikbin.kalaha.service.GameService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

/**
 * Controller of the game with receives requests and calls {@link GameService GameService}
 *
 * @author Behzad
 */
@RestController
@RequestMapping("/games")
class GameController(private val gameService: GameService) {

    @RequestMapping(value = ["/create"], method = [RequestMethod.POST])
    fun createGame(): ResponseEntity<GameCreateResponseDto> {
        val game = gameService.createGame()
        val dto = GameCreateResponseDto(game)
        return ResponseEntity.ok(dto)
    }

    @RequestMapping(value = ["/{gameId}/pits/{pitId}"], method = [RequestMethod.PUT])
    fun move(
            @PathVariable(value = "gameId") gameId: Long?,
            @PathVariable(value = "pitId") pitId: Int?
    ): ResponseEntity<GameMoveResponseDto> {
        val game = gameService.move(gameId, pitId)
        val dto = GameMoveResponseDto(game)
        return ResponseEntity.ok(dto)
    }

}
