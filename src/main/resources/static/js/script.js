//////////      global variables
let gameId = undefined;
let lastGameStatus = undefined;
let isMovingStepByStep = false;
//////////      end global variables

//////////      game config
const PLAYER_PITS_COUNT = 6;
const GAME_PITS_COUNT = PLAYER_PITS_COUNT * 2 + 2;      //  2 big pits
//////////      end game config

//////////      constants for parsing response
const PLAYER1_TURN = 'PLAYER1';
const PLAYER2_TURN = 'PLAYER2';
const PLAYER_TURN_CLASS = 'player-turn';
const DISABLED_PIT_CLASS = 'disabled-pit';
const GAME_NOT_FINISHED = 'NOT_FINISHED';
const GAME_PLAYER1_WON = 'PLAYER1_WON';
const GAME_PLAYER2_WON = 'PLAYER2_WON';
const GAME_FINISHED_EQUAL = 'FINISHED_EQUAL';
//////////      end constants for parsing response

//////////      constant selectors
const SELECTOR_PLAYER1_BOARD = '.player1-board';
const SELECTOR_PLAYER2_BOARD = '.player2-board';
const SELECTOR_GAME_BOARD = '.game-board';
const SELECTOR_CREATE_GAME_BUTTON = '#btn-create';
const SELECTOR_GAME_FINISH_STATE = '.game-finish-state';
const SELECTOR_PIT = '.pit';
const ATTR_DATA_PIT_NUM = 'data-pit-num';
//////////      end constant selectors

/*
    check if pit is a big pit
 */
function isBigPit(pitId) {
    return (pitId === GAME_PITS_COUNT / 2) || (pitId === GAME_PITS_COUNT);
}

/*
    returns id of the player(1/2)
 */
function getPlayerId(pitId) {
    return pitId <= GAME_PITS_COUNT / 2 ? 1 : 2;
}

/*
    maps (1-14) to (1-7)
 */
function getRelativePitId(pitId) {
    return pitId <= GAME_PITS_COUNT / 2 ? pitId : pitId - GAME_PITS_COUNT / 2;
}

/*
    check if pit is enabled for clicking
 */
function isEnabled(pitNumber, stoneCount, turn) {
    if (isBigPit(pitNumber)) {
        return false;
    }
    if (turn === PLAYER1_TURN) {
        if (pitNumber > PLAYER_PITS_COUNT) {
            return false;
        }
        return stoneCount !== 0;
    } else {
        if (pitNumber <= PLAYER_PITS_COUNT + 1) {
            return false;
        }
        return stoneCount !== 0;
    }
}

/*
    returns canvas of the given pitId
 */
function getCanvas(pitId) {
    return ($(".pit[data-pit-num='" + pitId + "'] canvas"))[0];
}

/*
    draws a stone at (distance, angle) in polar coordinates
    (w, h) is width and height of the canvas
 */
function drawStone(context, distance, angle, w, h) {
    //  constants
    const stoneArcRadius = Math.min(w, h) / 10.;

    //  convert to x, y coordinates
    let x = distance * Math.cos(angle);
    let y = distance * Math.sin(angle);

    //  draw
    context.beginPath();
    context.fillStyle = '#990303';
    context.arc(x, y, stoneArcRadius, 0, 2 * Math.PI, true);
    context.fill();
    context.beginPath();
    context.strokeStyle = 'black';
    context.lineWidth = 3;
    context.arc(x, y, stoneArcRadius, 0, 2 * Math.PI, true);
    context.stroke();
}

/*
    draws canvas and its contents
 */
function drawCanvas(canvas, pitId, stoneCount) {
    let w = canvas.width;
    let h = canvas.height;
    let r = Math.min(w, h) / 2.;
    let context = canvas.getContext('2d');
    //  move pivots to the center of canvas
    context.translate(w / 2., h / 2.);

    //  draw background
    context.beginPath();
    let bigPit = isBigPit(pitId);
    context.fillStyle = bigPit ? '#99a6b8' : '#b9c4ce';
    //  ellipse can draw both normal and big pit
    context.ellipse(0, 0, w / 2., h / 2., 0, 0, 2 * Math.PI, true);
    context.fill();

    context.beginPath();
    context.strokeStyle = '#3b424a';
    context.strokeWidth = 1;
    //  ellipse can draw both normal and big pit
    context.ellipse(0, 0, w / 2. - 1, h / 2. - 1, 0, 0, 2 * Math.PI, true);
    context.stroke();

    //  draw stones
    if (stoneCount === 1) {
        drawStone(context, 0, 0, w, h);
    } else {
        let angle = 0;
        const distance = r * 2 / 3.;
        const angleOffset = 2 * Math.PI / stoneCount;
        for (let i = 1; i <= stoneCount; i++) {
            drawStone(context, distance, angle, w, h);
            angle += angleOffset;
        }
    }

    //  draw label
    context.fillStyle = '#dd4c25';
    const coef = .42;
    let playerNum = getPlayerId(pitId);
    let relativePitId = getRelativePitId(pitId);
    let label = bigPit ? 'BP-' + playerNum : 'P' + playerNum + '-' + relativePitId;
    context.font = 'bold 30px Arial';
    context.fillText(label, -w * coef, -h * coef, 50);

    //  draw stone count (as text)
    context.fillStyle = '#3b424a';
    context.font = 'bold 40px Arial';
    let offsetX = stoneCount >= 10 ? 20 : 10;       //  handle text length
    context.fillText(stoneCount, -offsetX, 10, 60);

    //  return pivots back
    context.translate(-w / 2., -h / 2.);
}

function updateGamePitStep(command) {
    if (!command.hasOwnProperty('type') || !command.hasOwnProperty('to') || !command.hasOwnProperty('from')) {
        return;
    }
    const type = command.type;
    const from = command.from;
    const to = command.to;
    let fromStoneCount = lastGameStatus[from];
    let toStoneCount = lastGameStatus[to];
    if (type === 'MOVE_ONE') {
        fromStoneCount--;
        toStoneCount++;
    } else if (type === 'CAPTURE') {
        toStoneCount += fromStoneCount;
        fromStoneCount = 0;
    } else {
        console.log('Invalid command ' + type);
        return;
    }
    lastGameStatus[from] = fromStoneCount;
    lastGameStatus[to] = toStoneCount;
    let fromCanvas = getCanvas(from);
    let toCanvas = getCanvas(to);
    drawCanvas(fromCanvas, from, fromStoneCount);
    drawCanvas(toCanvas, to, toStoneCount);
}

/*
    Update board according to the given commands to show changes step by step which is more natural and easy to follow
 */
function updateGameSteps(commands) {
    if (!Array.isArray(commands)) {
        return;
    }
    if (lastGameStatus === undefined) {
        return;
    }

    let lastTimeout = 0;
    commands.forEach(function (item, index) {
        lastTimeout = 500 * index;
        setTimeout(function () {
            updateGamePitStep(item);
        }, lastTimeout);
    });
    return lastTimeout;
}

/*
    Updates the board according to the given status.
    Although the board is updated with steps, status is also used to be more <i>reliable</i> because steps are designed just to
    show step by step effect.
 */
function updateGameStatus(status, gameTurn, gameFinishState) {
    console.log(status);
    for (let i = 1; i <= GAME_PITS_COUNT; i++) {
        let stoneCount = status[i];
        let canvas = getCanvas(i);
        drawCanvas(canvas, i, stoneCount);

        let pitNode = $(".pit[data-pit-num='" + i + "']");
        if (isEnabled(i, stoneCount, gameTurn)) {
            pitNode.removeClass(DISABLED_PIT_CLASS);
        } else {
            pitNode.addClass(DISABLED_PIT_CLASS);
        }
    }
    if (gameTurn === PLAYER1_TURN) {
        $(SELECTOR_PLAYER1_BOARD).addClass(PLAYER_TURN_CLASS);
        $(SELECTOR_PLAYER2_BOARD).removeClass(PLAYER_TURN_CLASS);
    } else {
        $(SELECTOR_PLAYER1_BOARD).removeClass(PLAYER_TURN_CLASS);
        $(SELECTOR_PLAYER2_BOARD).addClass(PLAYER_TURN_CLASS);
    }
    if (gameFinishState !== GAME_NOT_FINISHED) {
        $(SELECTOR_GAME_FINISH_STATE).show();
        $(SELECTOR_CREATE_GAME_BUTTON).show();
        if (gameFinishState === GAME_PLAYER1_WON) {
            $(SELECTOR_GAME_FINISH_STATE).text('Player1 Won the Game!');
        } else if (gameFinishState === GAME_PLAYER2_WON) {
            $(SELECTOR_GAME_FINISH_STATE).text('Player2 Won the Game!');
        } else if (gameFinishState === GAME_FINISHED_EQUAL) {
            $(SELECTOR_GAME_FINISH_STATE).text('Game Finished Equal!');
        } else {
            $(SELECTOR_GAME_FINISH_STATE).text('Invalid Game State!');
        }
    }
    lastGameStatus = status;
}

/*
    update game according to server's response
 */
function updateGame(serverResponse) {
    if (!serverResponse.hasOwnProperty('status') ||
        !serverResponse.hasOwnProperty('gameTurn') ||
        !serverResponse.hasOwnProperty('gameFinishState')) {
        console.log('Server malformed response.');
        return;
    }
    let status = serverResponse.status;
    let gameTurn = serverResponse.gameTurn;
    let gameFinishState = serverResponse.gameFinishState;

    //  step by step move commands
    let stepByStepMoveTimeout = 0;
    if (serverResponse.hasOwnProperty('commands')) {
        //  disable clicks
        isMovingStepByStep = true;
        $(SELECTOR_PIT).addClass(DISABLED_PIT_CLASS);

        //  move step by step
        let commands = serverResponse.commands;
        stepByStepMoveTimeout = updateGameSteps(commands);
    }
    //  update game board
    setTimeout(function () {
        updateGameStatus(status, gameTurn, gameFinishState);
        //  enable clicks
        isMovingStepByStep = false;
    }, stepByStepMoveTimeout);

}

/*
    create a new game after pressing button
 */
function createGame() {
    console.log("Creating a new game.");
    $.ajax({
        type: "POST",
        url: '/games/create',
        success: function (serverResponse) {
            console.log('Game created.');
            if (serverResponse.hasOwnProperty('gameId')) {
                gameId = serverResponse.gameId;
            }
            updateGame(serverResponse);
            $(SELECTOR_CREATE_GAME_BUTTON).hide();
            $(SELECTOR_GAME_FINISH_STATE).hide();
            $(SELECTOR_GAME_BOARD).show();
        },
        contextType: 'application/json'
    });
}

/*
    | 1 | 2 | 3 | 4 | 5 | 6 | 7 (Big Pit 1) |
    ---------------------------------------------
    | 14 (Big Pit2) | 13 | 12 | 11 | 10 | 9 | 8 |
 */
function move(pitId) {
    console.log('Pit num clicked: ' + pitId);

    $.ajax({
        type: "PUT",
        url: '/games/' + gameId + '/pits/' + pitId,
        success: function (serverResponse) {
            console.log('Game create.' + serverResponse);
            updateGame(serverResponse);
        },
        contextType: 'application/json'
    });
}


/*
    document ready event (initial function)
 */
$(document).ready(function () {

    $(SELECTOR_GAME_BOARD).hide();
    $(SELECTOR_GAME_FINISH_STATE).hide();

    $(SELECTOR_CREATE_GAME_BUTTON).click(function () {
        createGame();
    });

    $('.pit:not(.big-pit)').each(function () {
        $(this).click(function () {
            if (isMovingStepByStep) {
                return;
            }
            let pitId = parseInt($(this).attr(ATTR_DATA_PIT_NUM));
            move(pitId);
        });
    });

    $(SELECTOR_PIT).each(function () {
        let pitId = parseInt($(this).attr(ATTR_DATA_PIT_NUM));
        let title = 'Player' + getPlayerId(pitId);
        title += ' ' + (isBigPit(pitId) ? 'BigPit' : 'Pit' + getRelativePitId(pitId));
        $(this).attr('title', title);
    });
});
